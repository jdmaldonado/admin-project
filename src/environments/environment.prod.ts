export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCUCJJR5FZBCHFgd2tIvlmHNxJw34ZATbA',
    authDomain: 'cpc-admin.firebaseapp.com',
    databaseURL: 'https://cpc-admin.firebaseio.com',
    projectId: 'cpc-admin',
    storageBucket: 'cpc-admin.appspot.com',
    messagingSenderId: '525242840397'
  }
};
