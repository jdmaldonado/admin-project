import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/** Guards */
import { AuthGuard } from './guards/auth.guard';
/** Components */
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { FormPropertyComponent } from './components/property/form-property/form-property.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: HomeComponent,
    canActivate: [ AuthGuard ],
    children: [
      { path: '', component: FormPropertyComponent }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
