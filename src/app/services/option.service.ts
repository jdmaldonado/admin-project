import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OptionService {

  constructor(private http: HttpClient) { }

  getCitiesByStateName(stateName: string): Observable<any> {
    return this.http.get('assets/jsons/colombia.json')
      .pipe(
        map((items: Array<any>) => {
          return items.find((state: any) => state.departamento === stateName);
        }),
        map((state: any) => state.ciudades)
      );
  }

  getStates(): Observable<any[]> {
    return this.http.get('assets/jsons/colombia.json')
      .pipe(
        map((items: Array<any>) => {
          return items.map(item => ({ id: item.id, name: item.departamento }));
        })
      );
  }
}
