import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Property } from '../models/property';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  properties: AngularFirestoreCollection<Property>;

  constructor(private db: AngularFirestore) {
    this.properties = db.collection<Property>('properties');
  }

  addProperty(propertyForm: any): Promise<any> {
    const property = this.mapFormToProperty(propertyForm);
    return this.properties.add(property);
  }

  private mapFormToProperty(propertyForm: any): Property {
    const property: Property = {
      address: propertyForm.address,
      area: propertyForm.area,
      chip: propertyForm.chip,
      city: propertyForm.city,
      colleage: propertyForm.colleage,
      country: propertyForm.country,
      description: propertyForm.description,
      name: propertyForm.name,
      priceM2: propertyForm.priceM2,
      registration: propertyForm.registration,
      state: propertyForm.state,
      stratum: propertyForm.stratum,
      type: propertyForm.type,
    };
    return property;
  }
}
