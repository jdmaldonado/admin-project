export interface Property {
  address: string;
  area: number;
  chip: string;
  city: string;
  colleage: string; // Colega - Asesor
  country: string;
  description: string;
  name: string;
  priceM2: number;
  registration: string; // Matrícula #
  state: string;
  stratum: string; // Estrato
  type: string;
}
