import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

/** Services */
import { PropertyService } from 'src/app/services/property.service';
import { OptionService } from 'src/app/services/option.service';

@Component({
  selector: 'app-form-property',
  templateUrl: './form-property.component.html',
  styleUrls: ['./form-property.component.scss']
})
export class FormPropertyComponent implements OnInit {

  public propertyForm: FormGroup;
  public submitted: Boolean = false;
  public states: Observable<any[]>;
  public cities: Observable<any[]>;

  constructor(
    private formBuilder: FormBuilder,
    private propertyService: PropertyService,
    private optionsService: OptionService
  ) { }

  ngOnInit() {
    this.initForm();
    this.initOptions();
  }

  private initForm(): void {
    this.propertyForm = this.formBuilder.group({
      address: ['', Validators.required],
      area: [null, Validators.required],
      chip: [null],
      city: ['', Validators.required],
      colleage: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      country: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      priceM2: [null, Validators.required],
      registration: [null],
      state: ['', Validators.required],
      stratum: ['', Validators.required],
      type: ['', Validators.required],
    });
  }

  private initOptions(): void {
    this.states = this.optionsService.getStates();
  }

  get form() { return this.propertyForm.controls; }

  /** Get the cities when State changes */
  public onStateChanged(event): void {
    const stateName = event.value;
    this.cities = this.optionsService.getCitiesByStateName(stateName);
  }

  public saveProperty(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.propertyForm.invalid) {
      return;
    }

    this.propertyService.addProperty(this.propertyForm.value)
      .then(() => alert('Done!'))
      .catch((err) => console.log(`%c ${err.message}`, 'color: orange; font-weight: bold'));
  }

}
